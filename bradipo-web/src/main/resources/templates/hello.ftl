<#import "/spring.ftl" as spring/>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hello ${name}!</title>
</head>
<body>
    <h2>Hello ${name}!!!!!!!</h2>
    <h3><@spring.message "subtitle" /></h3>
    <h3><@spring.theme "key0" /></h3>

</body>
</html>